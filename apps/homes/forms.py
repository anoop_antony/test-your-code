from django.forms import ModelForm
from .models import *



class AddHomeForm(ModelForm):

    class Meta:
        model = Houses
        fields = ['adress', 'city', 'postal_code', 'price', 'sale_type']
