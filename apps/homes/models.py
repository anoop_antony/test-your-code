# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils import timezone

from autoslug import AutoSlugField
from image_cropping import ImageRatioField

class BaseClass(models.Model):

    owner = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Houses(BaseClass):

    slug = AutoSlugField(populate_from='title', unique=True,
                         always_update=True, null=True)
    title = models.CharField(max_length=150)
    adress = models.CharField(max_length=150)
    city = models.CharField(max_length=30)
    postal_code = models.CharField(max_length=30, null=True)
    input_datetime = models.DateTimeField(default=timezone.now)
    expiry_date = models.DateTimeField(default=timezone.now)
    commencement_time = models.DateTimeField(default=timezone.now)
    description = models.CharField(max_length=250, blank=True)
    number_bedrooms = models.CharField(max_length=10, blank=True)
    number_bathrooms = models.CharField(max_length=10, blank=True)
    price = models.FloatField(null=True, blank=True)
    choices = (('s', 'sale'), ('l', 'Rent'))
    sale_type = models.CharField(max_length=20, choices=choices, default='sale')
    tenants = (('b', 'Bachelors'), ('f', 'Family'), ('a', 'Anyone'))
    tenants_type = models.CharField(max_length=20, choices=tenants, default='Anyone')
    house_image = models.ImageField(upload_to='house_images', null=True, blank=True)
    published = models.BooleanField(default=False)
    cropped_image = ImageRatioField('house_image', '430x360')

    class Meta:
        indexes = [
            models.Index(fields=['title', 'city']),
        ]

        verbose_name = "house"
        verbose_name_plural = "houses"

    def __unicode__(self):
        return self.adress

    def get_absolute_url(self):
        return reverse('homes:home_detail', kwargs={'slug': self.slug})

    def is_available(self):
        return self.expiry_date > timezone.now()

    def is_commenced(self):
        return self.commencement_time < timezone.now()


class LikeData(BaseClass):
    house = models.ForeignKey(Houses)

    class Meta:
        verbose_name = "like"
        verbose_name_plural = "likes"

    def __unicode__(self):
        return self.house.title
