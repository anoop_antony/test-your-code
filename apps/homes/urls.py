from django.conf.urls import url
from .views import *

####---url patterns for calling redirecting to different views with associated namespaces--####

urlpatterns = [
    url(r'^listing/', HomesListing.as_view(), name='listing'),

    url(r'^addcreate', AddCreate.as_view(), name='adcreate'),

    url(r'^homedetail/(?P<slug>[\w-]+)/$', HomeDetail.as_view(),
        name='home_detail'),

    url(r'^update/(?P<slug>[\w-]+)/$', HomeUpdate.as_view(),
        name='home_update'),

    url(r'^like_homes/', LikeHomes.as_view(), name='like_homes'),

    url(r'^favourites/', FavouriteHomes.as_view(),
        name='fav_homes'),

    url(r'^myposts/', MyPosts.as_view(), name='my_posts'),

    url(r'^search/', SearchHomes.as_view(), name='home_search'),

    url(r'^myaccount/(?P<pk>[\w-]+)>', MyAccount.as_view(),
        name='my_account'),

    url(r'makepaymet/(?P<pk>[\d]+)/$', MakePayment.as_view(),
        name='payment'),
]
