# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-11-06 12:11
from __future__ import unicode_literals

import autoslug.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('homes', '0004_auto_20171106_1204'),
    ]

    operations = [
        migrations.AlterField(
            model_name='houses',
            name='slug',
            field=autoslug.fields.AutoSlugField(editable=False, null=True, populate_from=b'city', unique_with=(b'title',)),
        ),
    ]
