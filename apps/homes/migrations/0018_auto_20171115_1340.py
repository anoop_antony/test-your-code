# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-11-15 13:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homes', '0017_auto_20171115_0902'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Agents',
        ),
        migrations.DeleteModel(
            name='Office',
        ),
        migrations.AlterModelOptions(
            name='houses',
            options={'verbose_name': 'house', 'verbose_name_plural': 'houses'},
        ),
        migrations.AlterModelOptions(
            name='likedata',
            options={'verbose_name': 'like', 'verbose_name_plural': 'likes'},
        ),
        migrations.RemoveField(
            model_name='houses',
            name='deleted_at',
        ),
        migrations.RemoveField(
            model_name='likedata',
            name='deleted_at',
        ),
        migrations.AddIndex(
            model_name='houses',
            index=models.Index(fields=['title', 'city'], name='homes_house_title_c01660_idx'),
        ),
    ]
