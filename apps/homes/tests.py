# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import timedelta
from django.contrib.auth import get_user_model
from django.utils import timezone

from apps.homes.models import Houses, LikeData
from listinghomes.tests.common import BaseTestCase

User = get_user_model()


class TestHomesListing(BaseTestCase):
    """
    Test Cases for homes
    """
    MODEL = Houses

    def test_create_house(self):
        """
        - Add a house and check if successfully creating a house.
        :return:
        """
        house = Houses.objects.create(
            owner=self.get_dummy_user(),
            title='New House',
            adress='Address line 1',
            city='New City',
            postal_code='689011',
            price=50.0,
        )
        self.assertIsInstance(house, Houses)

    def test_publish_house(self):
        """
        - Publish a house.
        :return:
        """
        house = self.get_dummy_object(self.MODEL)
        house.published = True
        house.save()
        self.assertTrue(house.published)

    def test_unpublish_house(self):
        """
        - Unpublish a house.
        :return:
        """
        house = self.get_dummy_object(self.MODEL)
        house.published = False
        house.save()
        self.assertFalse(house.published)

    def test_update_house(self):
        """
        - Edit existing values of a house.
        :return:
        """
        house_kwargs = {
            "title": "Test House",
            "adress": "Kakkanad",
            "city": "Kochi"
        }
        house = self.get_dummy_object(self.MODEL, **house_kwargs)
        house.title = "House 123"
        house.save()

        instance = Houses.objects.get(pk=house.pk)
        self.assertEqual(instance.title, 'House 123')

    def test_house_image(self):
        """
        - Update a new image for a house.
        :return:
        """
        house = self.get_dummy_object(self.MODEL)
        house.house_image = self.get_dummy_image()
        house.save()

        image_path = house.house_image.path
        self.failUnless(open(image_path), 'File not found')

    def test_expired_house(self):
        """
        - Check a house that was already expired is removed from listing.
        :return:
        """
        yesterday = timezone.now() - timedelta(days=1)
        house = self.get_dummy_object(self.MODEL)
        house.expirty_date = yesterday
        house.save()

        self.assertFalse(house.is_available())

    def test_house_commencement(self):
        """
        - Check a house whether it is available in listing only after date of commencement.
        :return:
        """
        yesterday = timezone.now() - timedelta(days=1)
        house = self.get_dummy_object(self.MODEL)
        house.commencement_time = yesterday
        house.save()

        self.assertTrue(house.is_commenced())

    def test_delete_house(self):
        """
        - Delete a house from database.
        :return:
        """
        print("Testing Deleting a House------------------------")
        house = self.get_dummy_object(self.MODEL)
        house.delete()
        self.assertIsNone(house.id)

    def test_likes(self):
        """
        - Check the likes count.
        :return:
        """
        print("Testing Liking a House------------------------")
        house = self.get_dummy_object(self.MODEL)
        like = LikeData.objects.get_or_create(owner=self.get_dummy_user(), house=house)
        self.assertIsInstance(like, LikeData)

    def test_dislikes(self):
        """
        - Check the dis-likes.
        :return:
        """
        print("Testing Disliking a House------------------------")
        house = self.get_dummy_object(self.MODEL)
        like = LikeData.objects.get_or_create(owner=self.get_dummy_user(), house=house)
        like.delete()
        self.assertIsNone(like.id)
