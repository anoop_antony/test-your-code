# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from .models import *
# Register your models here.


class HousesAdmin(admin. ModelAdmin):
    model = Houses
    list_display = ('adress', 'city', 'price')
admin.site.register(Houses, HousesAdmin)


class LiKeDataAdmin(admin.ModelAdmin):
    model = LikeData

admin.site.register(LikeData, LiKeDataAdmin)