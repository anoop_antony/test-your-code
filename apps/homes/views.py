# -*- coding: utf-8 -*-

# future imports
from __future__ import unicode_literals

##django imports
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin
from utils.helpers import LoggedInMixin, search_homes, complete_payment
from django.views.generic import TemplateView
from django.core.urlresolvers import reverse_lazy
from django.views import View

#custom imports
from .forms import *
import listinghomes.settings as sts

#third party imports
import stripe

#conatsnats and keys
stripe.api_key = sts.STRIPE_SECRET_KEY


class HomesListing(LoginRequiredMixin, ListView):
    """
    A class based view for listing all homes that are published.
    user permission:only logged in users
    """
    model = Houses
    template_name = 'houses_list.html'
    paginate_by = 8

    #queryset for fetching the model data
    def get_queryset(self):
        queryset = Houses.objects.filter(published=True)
        return queryset

homelist = HomesListing.as_view()


class AddCreate(LoggedInMixin, CreateView):

    """
    Generic Createview for creating new Ads.
    Render  CreateForm and validates it,
    user permission:only logged in users
    """
    model = Houses
    fields = ['title', 'adress', 'city', 'price',
              'sale_type', 'description', 'number_bedrooms',
              'number_bathrooms', 'house_image', 'tenants_type']

    template_name = 'houses_form.html'

    #function for validating the form
    def form_valid(self, form):
        new_house = form.save(commit=False)
        new_house.house_image = form.cleaned_data['house_image']
        new_house.owner = self.request.user
        new_house.save()
        return super(AddCreate, self).form_valid(form)

    #returns the url to redirect on successfull object creation
    def get_success_url(self):
        return reverse_lazy('homes:payment', kwargs={'pk': self.object.id})


class HomeDetail(LoggedInMixin, DetailView):
    """
    Generic detailView for returning details associated with each home
    user permission : only logged in users
    Tekes unique Slug value and returns the associated house object
    """
    model = Houses
    template_name = 'house_detail.html'

    #returns the context and any extra data associated with that
    def get_context_data(self, **kwargs):
        context = super(HomeDetail, self).get_context_data(**kwargs)
        context['title'] = 'Home Detail - {}'.format(self.get_object().title)
        return context


class LikeHomes(LoggedInMixin, View):
    """
    A generic view for retruning the like data of a house object by any user
    Takes in house-id and user id and returns if the user has already liked house or not
    Takes in data through ajax requests from detail page of each house object.
    user permission : only logged in users can access this view
    """

    def get(self, request):
        house = get_object_or_404(Houses, id=request.GET.get('house_id'))
        change = int(request.GET.get('state'))
        context = {'status': 0}

        # checks if the user has clicked the like button or not
        if change:
            obj, created = LikeData.objects.get_or_create(
                            owner=request.user,
                            house=house)
            if not created:
                obj.delete()
        #sends a status of 1 if he has already liked, else by default the status is 0.
        if LikeData.objects.filter(owner=self.request.user,
                                   house=house).exists():
                context['status'] = 1

        return JsonResponse(context)


class FavouriteHomes(LoggedInMixin, ListView):
    """
    ListView generic class for displaying the favourite homes of user.
    Returns the homes that are liked by logged-in user
    user permission : only logged in users
    """
    model = LikeData
    template_name = 'favourite_homes.html'

    #Returns the Likedata objects related to loggged in user
    def get_queryset(self):
        queryset = LikeData.objects.filter(owner=self.request.user)
        return queryset


class MyPosts(LoggedInMixin, HomesListing):
    """
    A generic class inherited from HomeListing ListView class
    returns all the ads that are posted by the logged in user
    user permission : only logged in users
    """
    def get_queryset(self):
        queryset = Houses.objects.filter(owner=self.request.user)
        return queryset


class SearchHomes(LoggedInMixin, HomesListing):
    """
    A generic class inherited from HomeListing ListView class
    For searching the homes based on different search queries
    user permission : only logged in users
    """

    #for returning the search results
    def get_queryset(self):
        location = self.request.GET.get('city')
        keyword = self.request.GET.get('keyword')
        home_type = self.request.GET.get('sel-type')
        context_query = search_homes(location, keyword, home_type)
        return context_query

    #returns the context with extra parameters for paginating the search results
    def get_context_data(self, **kwargs):
        context = super(SearchHomes, self).get_context_data(**kwargs)
        context['search'] = True
        context['location'] = self.request.GET.get('city')
        context['keyword'] = self.request.GET.get('keyword')
        context['seltype'] = self.request.GET.get('sel-type')
        return context


class MyAccount(LoggedInMixin, DetailView):
    """
    A generic DetailView for dispalying user details
    Returns both published and pending ads by the user
    user permission : only logged in users
    """

    template_name = 'about.html'
    model = User

    # returns all published and pending homes by the user through context
    def get_context_data(self, **kwargs):
        context = super(MyAccount, self).get_context_data(**kwargs)
        context['homes'] = Houses.objects.filter(owner=self.request.user)
        return context


class HomeUpdate(LoggedInMixin, UpdateView):
    """
    UpdateView for making changes to already added house object.
    Takes the house object through unique slug
    user permission : only logged in users
    Renders a prefilled form with current values of the object.
    """

    model = Houses
    fields = ['title', 'adress', 'city', 'price', 'sale_type',
              'description', 'number_bedrooms',
              'number_bathrooms', 'house_image']

    template_name = 'edit_home_details.html'

    #returns the url to redirect on successfull object updation
    def get_success_url(self):
        return reverse_lazy('homes:home_detail', kwargs={'slug': self.object.slug})


class MakePayment(LoggedInMixin, TemplateView):
    """
    Template view for completing the payment after posting Ads
    Renders a Form for getting the payment details and validates it
    Complete the payment using Stripe payment gateway
    """
    template_name = 'payment.html'

    def get_context_data(self, **kwargs):
        context = super(MakePayment, self).get_context_data(**kwargs)
        context['id'] = self.kwargs['pk']
        context['title'] = 'payment'
        return context

    #getting the payment details from form and completing the payment
    def post(self, request, **kwargs):
        pay = complete_payment(request, kwargs)
        return pay
