# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy, reverse
from django.views import View
from django.shortcuts import render

#django mports
from django.views.generic import FormView, UpdateView

#custom imports
from .forms import UserCreateForm, PhotoForm
from .models import UserProfile
from utils.helpers import LoggedInMixin


class Login(View):
    def get(self, request):
        return render(request, 'login.html')


class SignUp(FormView):
    """
    A form based generic view for user registration
    Takes the user information from and validates the data
    Authenticate the user if form is valid and returns the success url
    """

    form_class = UserCreateForm
    template_name = 'signup.html'

    #form validation and user authentication
    def form_valid(self, form):
        form.save()
        username = form.cleaned_data.get('username')
        raw_password = form.cleaned_data.get('password1')
        user = authenticate(username=username, password=raw_password)
        login(self.request, user)
        return HttpResponseRedirect(self.get_success_url())

    #return the redirect link after user creation
    def get_success_url(self):
        return reverse('homes:listing')


class UpdateProfile(LoggedInMixin, UpdateView):
    """
    Update view for changing the basic user information
    """
    model = UserProfile
    fields = ['first_name', 'contact_number', 'avatar', ]
    template_name = 'update_profile.html'

    def get_success_url(self):
        return reverse_lazy('homes:my_account', kwargs={'pk': self.object.id})


class ChangeAvatar(UpdateView):
    template_name = 'photo_upload.html'
    model = UserProfile
    form_class = PhotoForm

    # def form_valid(self, form):
    #     form.save()
    #     return super(ChangeAvatar,self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('homes:my_account',kwargs={'pk':self.request.user.id})