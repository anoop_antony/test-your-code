# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

from .forms import UserCreateForm

from faker import Faker


class AuthAccountsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.fake = Faker()
        self.fake.seed(4321)
        self.credentials = {
            'username': 'username',
            'password': 'secret',
            'is_active': 1}
        self.user = User.objects.create_user(**self.credentials)

    def test_login(self):
        """
        - check login works if username and password is correct.
        :return:
        """
        print("Testing user login *********************************")
        self.client.login(username=self.credentials['username'], password=self.credentials['password'])
        response = self.client.get('/', {'user_id': self.user.id})
        self.assertEqual(response.status_code, 200)

    def test_signup(self):
        """
        - Create a  new user in the database
        - Log the created user
        :return:
        """
        print("Testing user signup *********************************")
        form_data = {
            'username': 'username1',
            'password1': 'secret123',
            'password2': 'secret123',
            'email': self.fake.email()
        }
        form = UserCreateForm(data=form_data)
        self.assertTrue(form.is_valid())
        form.save()
        user = authenticate(username=form_data['username'], password=form_data['password1'])
        self.assertTrue(user.is_authenticated())

    def test_update_profile(self):
        """
        - Create a  new user in the database
        - Update the new user details
        :return:
        """
        print("Testing updation of user profile *********************************")
        first_name = self.fake.name().split(' ')[0]
        last_name = self.fake.name().split(' ')[1]
        self.user.first_name = first_name
        self.user.last_name = last_name
        self.user.save()
        self.assertEqual(self.user.first_name, first_name)
        self.assertEqual(self.user.last_name, last_name)

    def test_update_avatar(self):
        """
        - Create a  new user in the database
        - Upload an image to the newly created user
        :return:
        """
        pass