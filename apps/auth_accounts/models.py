# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    first_name = models.CharField(max_length=75, null=True)
    last_name = models.CharField(max_length=75, null=True)
    contact_number = models.CharField(max_length=150)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    avatar = models.ImageField(upload_to='avatars', blank=True, null=True)

    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            profile, created = UserProfile.objects.get_or_create(user=instance)

    post_save.connect(create_user_profile, sender=User)

    def __unicode__(self):
        return self.user.username

    class Meta:
        verbose_name = "profile"
        verbose_name_plural = "profiles"