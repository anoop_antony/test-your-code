from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.forms import FileInput
from apps.auth_accounts.models import UserProfile
from PIL import Image



class UserCreateForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user

class PhotoForm(forms.ModelForm):
    x_value=forms.FloatField(widget=forms.HiddenInput())
    y_value=forms.FloatField(widget=forms.HiddenInput())
    width=forms.FloatField(widget=forms.HiddenInput())
    height=forms.FloatField(widget=forms.HiddenInput())

    class Meta:
        model=UserProfile
        fields=['x_value','y_value','width','height','avatar']

    def save(self):
        photo=super(PhotoForm, self).save()
        x_value=self.cleaned_data.get('x_value')
        print("xvalue",x_value)
        y_value=self.cleaned_data.get('y_value')
        print("yvalue",y_value)
        width=self.cleaned_data.get('width')
        print("width",width)
        height=self.cleaned_data.get('height')
        print("height",height)

        image=Image.open(photo.avatar)
        cropped_image = image.crop((x_value, y_value, width+x_value, height+y_value))
        resized_image = cropped_image.resize((400, 400), Image.ANTIALIAS)
        resized_image.save(photo.avatar.path)

        return photo