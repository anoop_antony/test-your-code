# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import UserProfile

#
# class CustomUserAdmin(admin.ModelAdmin):
#     model=CustomUser
#     list_display = ('name', 'email', 'phone_number')


#admin.site.register(UserProfile)

class UserProfileAdmin(admin.ModelAdmin):
    model = UserProfile
    list_display = ('user', 'first_name', 'contact_number')
admin.site.register(UserProfile,UserProfileAdmin)