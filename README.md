# README #

### Project summary ###

* A Django based web application for posting and finding the ads about homes for rent and sale.
* Any user can log-in/sign-up and post an ad by pasying 10$
* Interested users can search and shortlist the ads.
* Completer details of each ad is available.


### Basic Requirements ###

* The project will run on any machine with the following requirements
* python >=3.0
* postgres
* virtualenv
* other dependencies are mentioned in requirements.txt

### Running the Project ###

* clone the repo
* activate the virtualenv
* install the packages from requirements.txt using pip
* Move to project root
* django comes in with default Sqllite db.
* if using postgres sql configure the db and make apropriate changes in settings.py file

