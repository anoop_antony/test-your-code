
import string
import time
from random import Random, randint

#django imports
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.decorators import method_decorator

#custom imports
from apps.homes.models import Houses

#third party imports
import stripe


#function for searching the houses based on query statuses
def search_homes(city, keyword, home_type):

    if home_type == "both":
        filtered_query = Houses.objects.filter(published=True)
    else:
        filtered_query = Houses.objects.filter(sale_type=home_type, published=True)

    if city:
        filtered_query = filtered_query.filter(city__icontains=city)

    if keyword:
        filtered_query = filtered_query.filter(title__icontains=keyword)

    return filtered_query


class LoggedInMixin(object):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoggedInMixin, self).dispatch(*args, **kwargs)


def complete_payment(request, kwargs):
    token = request.POST['stripeToken']

    try:
        charge = stripe.Charge.create(
            amount=1000,
            currency="usd",
            description="example",
            source=token,)

    except Exception as e:
        return render(request, 'error.html', {'error': e.code})

    if charge['status'] == "succeeded":
        home_id = kwargs['pk']
        Houses.objects.filter(id=home_id).update(published=True)
        slug = Houses.objects.get(id=home_id).slug
        return render(request, 'success.html', {'slug': slug, 'charge': charge})
    else:
        return render(request, 'error.html', )


class UtlRandom:
    '''
    Random # generation
    '''
    random = ""

    def __init__(self):
        self.random = Random(x=time.time())

    def random_str(self, n):
        return ''.join(self.random.choice(string.ascii_uppercase +
                                          string.digits) for _ in range(n))

    def random_num(self, b, e):
        return self.random.randint(b, e)

    def random_username(self, length):
        return ''.join(self.random.choice(string.lowercase) for i in range(length))

    def random_email(self, length):
        email_host = 'example.com'
        random_username = self.random_username(length)
        return random_username+'@'+email_host