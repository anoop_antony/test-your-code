"""
    list of project urls

"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static
from listinghomes import settings
from apps.homes.views import homelist


urlpatterns = [

    ###########---------------Urls for apps and redirects-----------#########

    url(r'^$', homelist, name="redirect"),
    url(r'^admin/', admin.site.urls),
    url(r'^homes/', include('apps.homes.urls', namespace='homes')),
    url(r'^accounts/', include('apps.auth_accounts.urls', namespace='accounts')),
    url(r'^logout/$', auth_views.LogoutView.as_view(), name='logout'),




    ###########---------------Urls for password reset and upates-----------#########

    url(r'^settings/password/$',
        auth_views.PasswordChangeView.as_view(
        template_name='password_change.html'),
        name='password_change'),


    url(r'^settings/password/done/$',
        auth_views.PasswordChangeDoneView.as_view(
        template_name='password_change_done.html'),
        name='password_change_done'),
]

 ###########---------------Urls for loading media and static files-----------#########

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)