import os
import tempfile

from django.test import TestCase, Client, RequestFactory
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from model_mommy import mommy
from io import BytesIO
from PIL import Image
from faker import Faker

from apps.auth_accounts import models as account_models


class BaseTestCase(TestCase):

    fixtures = []
    tmp_dir = tempfile.mkdtemp()
    request = RequestFactory()
    client = Client()
    fake = Faker()

    @staticmethod
    def get_dummy_object(model,
                         _persistent=True,
                         _save_related=True,
                         _quantity=None,
                         _fill_optional=False,
                         _save_kwargs={},
                         **kwargs):
        """Returns an object based on model

        _persistent=False: returns an object without saving it to db
        _save_related: when _persistent=False should it save the nested relations or not
        _quantity: Amount of objects to create

        By default, model-mommy skips fields with null=True or blank=True. Also
        if a field has a default value, it will be used _fill_optional = Amount
        of objects to create

        _fill_optional=['happy', 'bio']: a list of fields to fill with random data
        _fill_optional=True: fill all fields with random data

        Use kwargs to override default 'random' values
        """
        if _persistent:
            return mommy.make(model,
                              _quantity=_quantity,
                              _fill_optional=_fill_optional,
                              _save_kwargs=_save_kwargs,
                              **kwargs)
        else:
            return mommy.prepare(model,
                                 _save_related=_save_related,
                                 _quantity=_quantity,
                                 _fill_optional=_fill_optional,
                                 _save_kwargs=_save_kwargs,
                                 **kwargs)

    @classmethod
    def get_dummy_user(cls, password=None, permissions=None, **kwargs):

        user = cls.get_dummy_object(account_models.User, **kwargs)

        if password:
            user.set_password(password)
            user.save()

        return user

    @classmethod
    def get_dummy_profile(cls, profile_class, **kwargs):

        password = kwargs.pop('password', None)

        if password:
            kwargs['user'] = cls.get_dummy_user(password=password)

        return cls.get_dummy_object(profile_class, **kwargs)

    @staticmethod
    def get_dummy_image():
        stream = BytesIO()
        image = Image.new('RGB', (100, 100))
        image.save(stream, format='jpeg')
        return SimpleUploadedFile("file.jpg", stream.getvalue(), 'image/jpeg')

    @staticmethod
    def get_dummy_file(filename='file.xxx', content=b'bytes of the file'):
        return SimpleUploadedFile(filename, content, 'image/jpeg')

    def login(self, profile, password=None):
        if not password:
            password = 'password'
            profile.user.set_password(password)
            profile.user.save()

        credentials = {
            'username': profile.user.username,
            'password': password
        }

        response = self.client.post(reverse("accounts:login"), credentials, follow=True)
        self.assertTrue(response.context['user'].is_active)

    def tearDown(self):
        for file_name in os.listdir(BaseTestCase.tmp_dir):
            file_path = os.path.join(BaseTestCase.tmp_dir, file_name)
            os.remove(file_path)
